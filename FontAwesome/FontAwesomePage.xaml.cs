﻿using Xamarin.Forms;
using Plugin.Iconize;
using FormsPlugin.Iconize;
namespace FontAwesome
{
    public partial class FontAwesomePage : ContentPage
    {
        public FontAwesomePage()
        {
            InitializeComponent();
            Content = new FormsPlugin.Iconize.IconImage
            {
                Icon = "fa-adjust",
                IconColor = Color.Blue,
            };
        }
    }
}
