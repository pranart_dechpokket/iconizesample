﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;


namespace FontAwesome.Droid
{
    [Activity(Label = "FontAwesome.Droid", Icon = "@drawable/icon", Theme = "@style/MyTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.FontAwesomeModule());
            Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.MaterialModule());
            Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.EntypoPlusModule());
            Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.IoniconsModule());
            Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.MeteoconsModule());
            Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.SimpleLineIconsModule());
            Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.TypiconsModule());
            Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.WeatherIconsModule());

            FormsPlugin.Iconize.Droid.IconControls.Init(Resource.Id.toolbar, Resource.Id.tabMode);
            LoadApplication(new App());
        }
    }
}
