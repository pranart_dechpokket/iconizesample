﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using FontAwesome;

namespace FontAwesome.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();

            // Code for starting up the Xamarin Test Cloud Agent
#if DEBUG
			Xamarin.Calabash.Start();
#endif
            Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.FontAwesomeModule());
            Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.MaterialModule());
            Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.EntypoPlusModule());
            Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.IoniconsModule());
            Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.MeteoconsModule());
            Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.SimpleLineIconsModule());
            Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.TypiconsModule());
            Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.WeatherIconsModule());

            FormsPlugin.Iconize.iOS.IconControls.Init();
            LoadApplication(new App());

            return base.FinishedLaunching(app, options);
        }
    }
}
